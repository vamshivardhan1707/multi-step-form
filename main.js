const nextButtonStep1 = document.getElementById('Next-1');
const nextButtonStep2 = document.getElementById('Next-2');
const nextButtonStep3 = document.getElementById('Next-3');
const confirmButtonStep4 = document.getElementById('Next-4');
const changeButtonStep4 = document.querySelector('.plan-selected a')
const yourInfoSection = document.getElementById('YourInfo');
const selectPlanSection = document.getElementById('SelectPlan');
const addOnsSection = document.getElementById('AddOns');
const finishingUpSection = document.getElementById('FinishingUp');
const thankyouSection = document.getElementById('ThankYou');
const toggleSwitch = document.getElementById('toggle-button');
const toggleText = document.querySelectorAll('#toggle #text');
const monthlyElements = document.querySelectorAll('#Monthly');
const yearlyElements = document.querySelectorAll('#Yearly');
const stepNumbersContainer = document.querySelectorAll('aside .StepNumbers span');
const goBackButtonStep2 = document.querySelector('.Links-2 a');
const goBackButtonStep3 = document.querySelector('.Links-3 a');
const goBackButtonStep4 = document.querySelector('.Links-4 a');
const content1Div = document.querySelector('.Content-1');
const content2Div = document.querySelector('.Content-2');
const checkboxesStep3 = document.querySelectorAll('.Options input[type="checkbox"]');
const addonsSelectedDiv = document.querySelector('#FinishingUp .addons-selected');
const nameInput = document.querySelector('#YourInfo input[type="text"]');
const emailInput = document.querySelector('#YourInfo input[type="email"]');
const phoneInput = document.querySelector('#YourInfo input[type="tel"]');
const nameError = document.querySelector('#YourInfo .name-error');
const emailError = document.querySelector('#YourInfo .email-error');
const phoneError = document.querySelector('#YourInfo .tel-error');
let selectedAddonDiv = document.createElement('div');
let planSelectedSpan = document.querySelector('#FinishingUp .Content-4 #plan-name');
let planPriceSpan = document.querySelector('#FinishingUp .Content-4 #plan-price');
let finalPriceSpan = document.querySelector('#FinishingUp .FinalPrice #final-price-number');
let finalPriceTextSpan = document.querySelector('#FinishingUp .FinalPrice #final-price-text');
let planName = '';
let planPrice = '';
let addonName = '';
let addonMonthlyPrice = '';
let addonYearlyPrice = '';
let monthlyPriceElement;
let yearlyPriceElement;
let selectedAddon;
let content2DivSelected = false;
let isFormValid = true;

content1Div.addEventListener('input', (event) => {
  const inputField = event.target;
  let errorSpan;

  if (inputField.type === 'text') {
    errorSpan = inputField.parentElement.querySelector('.name-error');
  } else if (inputField.type === 'email') {
    errorSpan = inputField.parentElement.querySelector('.email-error');
  } else if (inputField.type === 'tel') {
    errorSpan = inputField.parentElement.querySelector('.tel-error');
  }

  if (inputField.value.trim() !== '') {
    errorSpan.style.display = 'none';
    inputField.style.borderColor = "";
  }
});


function validateForm() {
  isFormValid = true;

  function validateInput(inputField, errorSpan, pattern) {
    if (inputField.value.trim() === '') {
      errorSpan.textContent = 'This field is required';
      errorSpan.style.display = 'inline';
      inputField.style.borderColor = "#DC4C6B";
      isFormValid = false;
    } else if (!pattern.test(inputField.value.trim())) {
      errorSpan.textContent = 'Invalid Input';
      errorSpan.style.display = 'inline';
      inputField.style.borderColor = "#DC4C6B";
      isFormValid = false;
    } else {
      errorSpan.style.display = 'none';
      inputField.style.borderColor = "";
    }
  }
  const namePattern = /^[A-Za-z\s]+$/;
  const emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/; 
  const phonePattern = /^\d{10}$/; 

  validateInput(nameInput, nameError, namePattern);
  validateInput(emailInput, emailError, emailPattern);
  validateInput(phoneInput, phoneError, phonePattern);
}

nextButtonStep1.addEventListener('click', () => {
  isFormValid = true;

  validateForm();
  
  if (isFormValid) {
    yourInfoSection.classList.add('hidden');
    selectPlanSection.classList.remove('hidden');

    stepNumbersContainer[0].style.backgroundColor = 'transparent'
    stepNumbersContainer[0].style.color = '#fff';
    stepNumbersContainer[1].style.backgroundColor = '#BFE1FD';
    stepNumbersContainer[1].style.color = '#05254E';
  };
});

function resetCheckboxes() {
  checkboxesStep3.forEach((checkbox) => {
    checkbox.checked = false;
    const optionDiv = checkbox.closest('.Options');
    const optionSpanMonthly = optionDiv.querySelector('#Monthly');
    const optionSpanYearly = optionDiv.querySelector('#Yearly');
    optionDiv.style.borderColor = "";
    optionSpanMonthly.style.color = "";
    optionSpanYearly.style.color = "";

    const selectedAddonDiv = addonsSelectedDiv.querySelector(`[data-addon="${checkbox.id}"]`);
    if (selectedAddonDiv) {
      addonsSelectedDiv.removeChild(selectedAddonDiv);
    };
  });
};

toggleSwitch.addEventListener('change', () => {
  if (toggleSwitch.checked) {
    monthlyElements.forEach(element => {
      element.style.display = 'none';
    });
    yearlyElements.forEach(element => {
      element.style.display = 'inline';
    });

    toggleText[0].style.color = 'gray';
    toggleText[1].style.color = '#05254E';

    planPrice = yearlyPriceElement.textContent;
    planSelectedSpan.textContent = `${planName} (Yearly)`;
    planPriceSpan.textContent = `${planPrice}`;

    updateFinalPrice();
    resetCheckboxes();
  } else {
    monthlyElements.forEach(element => {
      element.style.display = 'inline';
    });
    yearlyElements.forEach(element => {
      element.style.display = 'none';
    });

    toggleText[1].style.color = 'gray';
    toggleText[0].style.color = '#05254E';

    planPrice = monthlyPriceElement.textContent;
    planSelectedSpan.textContent = `${planName} (Monthly)`;
    planPriceSpan.textContent = `${planPrice}`;

    updateFinalPrice();
    resetCheckboxes();
  }
});

checkboxesStep3.forEach((checkbox) => {
  checkbox.addEventListener('change', () => {
    const optionDiv = checkbox.closest('.Options');
    const optionSpanMonthly = optionDiv.querySelector('#Monthly');
    const optionSpanYearly = optionDiv.querySelector('#Yearly');
    addonName = optionDiv.querySelector('.Options-Text h3').textContent;
    addonMonthlyPrice = optionDiv.querySelector('.Options #Monthly').textContent;
    addonYearlyPrice = optionDiv.querySelector('.Options #Yearly').textContent;

    if (checkbox.checked) {
      optionDiv.style.borderColor = "#4A3DFF";
      optionSpanMonthly.style.color = "#4A3DFF";
      optionSpanYearly.style.color = "#4A3DFF";

      selectedAddonDiv = addonsSelectedDiv.querySelector(`[data-addon="${checkbox.id}"]`);
      if (selectedAddonDiv) {
        selectedAddonDiv.innerHTML = `
          <div class="addonOptions">
            <span>${addonName}</span>
            <span>${toggleSwitch.checked ? addonYearlyPrice : addonMonthlyPrice}</span>
          </div>
        `;
      } else {
        const newAddonDiv = document.createElement('div');
        newAddonDiv.classList.add('selected-addon');
        newAddonDiv.dataset.addon = checkbox.id;
        newAddonDiv.innerHTML = `
          <div class="addonOptions">
            <span>${addonName}</span>
            <span>${toggleSwitch.checked ? addonYearlyPrice : addonMonthlyPrice}</span>
          </div>
        `;
        addonsSelectedDiv.appendChild(newAddonDiv);
      }

      updateFinalPrice();
    } else {
      optionDiv.style.borderColor = "";
      optionSpanMonthly.style.color = "";
      optionSpanYearly.style.color = "";

      const selectedAddonDiv = addonsSelectedDiv.querySelector(`[data-addon="${checkbox.id}"]`);
      if (selectedAddonDiv) {
        addonsSelectedDiv.removeChild(selectedAddonDiv);
      }

      updateFinalPrice();
    }
  });
});


goBackButtonStep2.addEventListener('click', () => {
  yourInfoSection.classList.remove('hidden');
  selectPlanSection.classList.add('hidden');

  stepNumbersContainer[0].style.backgroundColor = '#BFE1FD'
  stepNumbersContainer[0].style.color = '#05254E';
  stepNumbersContainer[1].style.backgroundColor = 'transparent';
  stepNumbersContainer[1].style.color = '#fff';
});

content2Div.addEventListener('click', (event) => {
  let clickedDiv = event.target.closest('.Arcade, .Advanced, .Pro');

  if (clickedDiv) {
    document.querySelectorAll('.Arcade, .Advanced, .Pro').forEach(div => {
      div.style.backgroundColor = '';
      div.style.borderColor = '';
      div.classList.remove('selected');
      content2DivSelected = true;
    });

    clickedDiv.style.borderColor = '#05254E';
    clickedDiv.style.backgroundColor = '#EEF5FF'
    clickedDiv.classList.add('selected');

    planName = clickedDiv.querySelector('h3').textContent;
    planSelectedSpan.textContent = `${planName} (Monthly)`;

    monthlyPriceElement = clickedDiv.querySelector('#Monthly');
    yearlyPriceElement = clickedDiv.querySelector('#Yearly');
    planPrice = monthlyPriceElement.textContent;
    planPriceSpan.textContent = `${planPrice}`;

    if (toggleSwitch.checked) {
      planSelectedSpan.textContent = `${planName} (Yearly)`;
      planPrice = yearlyPriceElement.textContent;
      planPriceSpan.textContent = `${planPrice}`;
    };

    updateFinalPrice();
    resetCheckboxes();
  };
});

nextButtonStep2.addEventListener('click', () => {
  if (content2DivSelected) {
    selectPlanSection.classList.add('hidden');
    addOnsSection.classList.remove('hidden')

    stepNumbersContainer[1].style.backgroundColor = 'transparent'
    stepNumbersContainer[1].style.color = '#fff';
    stepNumbersContainer[2].style.backgroundColor = '#BFE1FD';
    stepNumbersContainer[2].style.color = '#05254E';
  };
});

goBackButtonStep3.addEventListener('click', () => {
  selectPlanSection.classList.remove('hidden');
  addOnsSection.classList.add('hidden');

  stepNumbersContainer[1].style.backgroundColor = '#BFE1FD'
  stepNumbersContainer[1].style.color = '#05254E';
  stepNumbersContainer[2].style.backgroundColor = 'transparent';
  stepNumbersContainer[2].style.color = '#fff';
});

nextButtonStep3.addEventListener('click', () => {
  addOnsSection.classList.add('hidden');
  finishingUpSection.classList.remove('hidden');

  stepNumbersContainer[3].style.backgroundColor = '#BFE1FD'
  stepNumbersContainer[3].style.color = '#05254E';
  stepNumbersContainer[2].style.backgroundColor = 'transparent';
  stepNumbersContainer[2].style.color = '#fff';

  updateFinalPrice();
});

confirmButtonStep4.addEventListener('click', () => {
  finishingUpSection.classList.add('hidden');
  thankyouSection.classList.remove('hidden');
});

changeButtonStep4.addEventListener('click', () => {
  finishingUpSection.classList.add('hidden');
  selectPlanSection.classList.remove('hidden');

  stepNumbersContainer[1].style.backgroundColor = '#BFE1FD'
  stepNumbersContainer[1].style.color = '#05254E';
  stepNumbersContainer[3].style.backgroundColor = 'transparent';
  stepNumbersContainer[3].style.color = '#fff';
});

goBackButtonStep4.addEventListener('click', () => {
  finishingUpSection.classList.add('hidden');
  addOnsSection.classList.remove('hidden');

  stepNumbersContainer[2].style.backgroundColor = '#BFE1FD'
  stepNumbersContainer[2].style.color = '#05254E';
  stepNumbersContainer[3].style.backgroundColor = 'transparent';
  stepNumbersContainer[3].style.color = '#fff';
});

function updateFinalPrice() {
  const total = parseInt(planPrice.slice(1, -3)) + calculateAddonPrices();

  if (toggleSwitch.checked) {
    finalPriceSpan.textContent = `$${total}/yr`;
    finalPriceTextSpan.textContent = 'Total (per year)';
  } else {
    finalPriceSpan.textContent = `$${total}/mo`
    finalPriceTextSpan.textContent = 'Total (per month)';
  };

};

function calculateAddonPrices() {
  let addonPrices = 0;
  const selectedAddons = addonsSelectedDiv.querySelectorAll('.selected-addon');

  selectedAddons.forEach((addon) => {
    const addonPriceSpan = addon.querySelector('span:last-child');
    const addonPrice = parseInt((addonPriceSpan.textContent).slice(2, -3));
    addonPrices += addonPrice;
  });

  return addonPrices;
};


